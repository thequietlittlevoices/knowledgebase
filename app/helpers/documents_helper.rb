module DocumentsHelper
  def created_at_format(document)
    document.created_at.strftime("%d %b. %Y   %H:%M")
  end
end
