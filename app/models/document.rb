class Document < ActiveRecord::Base
  
  belongs_to :user
  has_and_belongs_to_many :words
  validates_presence_of :content, :title
  before_destroy {|document| document.delete_words}

  def delete_words
    self.words.each do |word|
       first_word = Word.find_by(content: word.content)
       first_word.decrement!(:count)
       if first_word.count==0
         first_word.destroy
       end
     end
     self.words.destroy_all
  end

  def add_words(words_list)
    words_list.each do |word|
      first_word = Word.find_by(content: word)
      if first_word.nil?
        new_word = Word.new(:content => word, :count => 1)
        new_word.save
        self.words<<new_word   
      else
        first_word.increment!(:count)
        self.words<<first_word
      end
    end
  end

end
