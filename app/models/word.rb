class Word < ActiveRecord::Base
  has_and_belongs_to_many :documents
  validates :count, numericality: true
  validates :content, format: { with: /\A[a-zA-Z]+\z/}
  validates_presence_of :content, :count
  
end
