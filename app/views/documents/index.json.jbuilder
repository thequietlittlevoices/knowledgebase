json.array!(@documents) do |document|
  json.extract! document, :id, :content,: title
  json.url document_url(document, format: :json)
end
