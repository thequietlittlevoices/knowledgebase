class DocumentsController < ApplicationController
  require 'html/sanitizer'

  before_action :set_document, only: [:show, :edit, :update, :destroy]
  before_filter :require_permission, only: [:edit, :update, :destroy]

  # GET /documents
  # GET /documents.json
  def index
    @my_documents = Document.where(:user_id => current_user).order(created_at: :desc)
    @other_documents = Document.where.not(:user_id => current_user).order(created_at: :desc)
    
  end

  # GET /documents/1
  # GET /documents/1.json
  def show
  end

  # GET /documents/new
  def new
    @my_document = Document.new
   
  end

  # GET /documents/1/edit
  def edit
  end

  # POST /documents
  # POST /documents.json
  def create
    @my_document = Document.new(my_document_params)
    @my_document.user_id = current_user.id

    respond_to do |format|
      if @my_document.save

        words_list = split_into_words(@my_document.content)
        @my_document.add_words(words_list) unless words_list.empty?

        format.html { redirect_to @my_document, notice: 'Document was successfully created.' }
        format.json { render :show, status: :created, location: @my_document }
      else
        format.html { render :new }
        format.json { render json: @my_document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/1
  # PATCH/PUT /documents/1.json
  def update
    my_document_old = @my_document

    respond_to do |format|
      if @my_document.update(my_document_params)

        my_document_old.delete_words
        words_list = split_into_words(@my_document.content)
        @my_document.add_words(words_list) unless words_list.empty?

        format.html { redirect_to @my_document, notice: 'Document was successfully updated.' }
        format.json { render :show, status: :ok, location: @document }
      else
        format.html { render :edit }
        format.json { render json: @my_document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    @my_document.delete_words

    @my_document.destroy
    respond_to do |format|
      format.html { redirect_to documents_url, notice: 'Document was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  
   def set_document
     @my_document = Document.find(params[:id])
   end

   def require_permission
     if current_user.id != Document.find(params[:id]).user_id
       redirect_to root_path
     end
   end
   
   def my_document_params
     params.require(:document).permit(:content, :title)
   end

   def split_into_words(content)
     full_sanitizer = HTML::FullSanitizer.new
     clean_text = full_sanitizer.sanitize(content)
     words_list = clean_text.split(/[\s,.;!?]/).select{|_w| _w =~ /^[a-zA-Z]+$/} unless clean_text.nil?
     #logger.debug "Word list: #{words_list}"
     return words_list    
   end

end
