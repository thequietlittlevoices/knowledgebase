class CreateJoinTableDocumentWord < ActiveRecord::Migration
  def change
    create_join_table :documents, :words do |t|
      # t.index [:document_id, :word_id]
       t.index [:word_id, :document_id]
    end
  end
end
