class ChangeContentFormatInDocuments < ActiveRecord::Migration
  def up
   change_column :documents, :content, :text
  end

  def down
   change_column :documents, :content, :string
  end
end
