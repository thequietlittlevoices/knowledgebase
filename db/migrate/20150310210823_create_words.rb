class CreateWords < ActiveRecord::Migration
  def change
    create_table :words do |t|
      t.string :content
      t.integer :count

      t.timestamps
    end
  end
end
