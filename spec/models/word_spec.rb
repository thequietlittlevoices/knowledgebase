require 'spec_helper'

describe Word do
  it { should have_and_belong_to_many(:documents) }
  it { should validate_presence_of(:content) }
  it { should validate_presence_of(:count) }
  it { should_not allow_value("test1").for(:content) }
  it { should allow_value("test").for(:content) }
  it { should validate_numericality_of(:count) }
end