require 'spec_helper'

describe User do
  it { should have_many(:documents) }
  it { should validate_uniqueness_of(:email) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password) }
  it { should validate_confirmation_of(:password) }
  it { should validate_length_of(:password).is_at_least(8) }
  it { should_not allow_value("test").for(:email) }
  it { should allow_value("test@test.com").for(:email) }
end