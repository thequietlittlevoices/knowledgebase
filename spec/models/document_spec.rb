require 'spec_helper'

describe Document do
  it { should belong_to (:user) }
  it { should have_and_belong_to_many(:words) }
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:content) }
end