require 'rails_helper'

describe DocumentsController do
  include Rails.application.routes.url_helpers
  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    user = FactoryGirl.create(:user)
    sign_in user
  end

  it "should have a current_user" do
    expect(subject.current_user).not_to be(nil)
  end

  it "should get index" do
    get 'index'
    response.should be_success
  end
end
